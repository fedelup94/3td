﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid
{
    private Vector3 offset;
    private int height;
    private int width;
    private float cellSize;
    private int[,] gridArray;
    private TextMesh[,] debugTextArray;

    public Grid(int width, int height, float cellSize, Vector3 offset = new Vector3())
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.offset = offset;

        gridArray = new int[width, height];
        debugTextArray = new TextMesh[width, height];
    }

    public void DrawLines()
    {
        for (int x = 0; x < gridArray.GetLength(0); ++x)
        {
            for (int y = 0; y < gridArray.GetLength(1); ++y)
            {

                //Debug.DrawLine(GetWorldPosition(roundedX + x, roundedY + y), GetWorldPosition(roundedX + x, roundedY + y + 1), Color.black, 100f);
                //Debug.DrawLine(GetWorldPosition(roundedX + x, roundedY + y), GetWorldPosition(roundedX + x + 1, roundedY + y), Color.black, 100f);
                debugTextArray[x,y] = GridManager.CreateWorldText(gridArray[x, y].ToString(), null, GetWorldPosition(x,y) + new Vector3(cellSize, 0, cellSize) *.5f, 20, Color.white, TextAnchor.MiddleCenter);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.black, 10000f, true);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.black, 10000f, true);
            }
        }

        SetValue(0, 0, 100);
    }

    public Vector2Int GetXY(Vector3 position)
    {
        int roundedX = Mathf.RoundToInt(offset.x);
        int roundedY = Mathf.RoundToInt(offset.z);
        Vector2Int temp = new Vector2Int();
        temp.x = Mathf.FloorToInt((position.x - roundedX) / cellSize);
        temp.y = Mathf.FloorToInt((position.z - roundedY) / cellSize);

        //Debug.Log(temp.x + "  " + temp.y);

        return temp;
    }

    public Vector3 GetWorldPosition(int x, int y)
    {
        int roundedX = Mathf.RoundToInt(offset.x);
        int roundedY = Mathf.RoundToInt(offset.z);
        Vector3 val = new Vector3(x, 0, y) * cellSize;
        val.x += roundedX;
        val.z += roundedY;
        val.y += offset.y;
        return val;
        //return new Vector3(x, 0, y) * cellSize;
    }

    public void SetValue(int x, int y, int value)
    {
        if (x >= 0 && y >= 0 && x < width && y < height)
        {
            gridArray[x, y] = value;
            debugTextArray[x, y].text = gridArray[x, y].ToString();
        }
    }

    public void SetValue(Vector3 worldPosition, int value)
    {
        Vector2Int pair = GetXY(worldPosition);
        SetValue(pair.x, pair.y, value);
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        for (int x = 0; x < gridArray.GetLength(0); ++x)
        {
            for (int y = 0; y < gridArray.GetLength(1); ++y)
            {

                //Debug.DrawLine(GetWorldPosition(roundedX + x, roundedY + y), GetWorldPosition(roundedX + x, roundedY + y + 1), Color.black, 100f);
                //Debug.DrawLine(GetWorldPosition(roundedX + x, roundedY + y), GetWorldPosition(roundedX + x + 1, roundedY + y), Color.black, 100f);
                Gizmos.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1));
                Gizmos.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y));
            }
        }
    }

}
