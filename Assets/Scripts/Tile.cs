﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour
{

    [SerializeField]
    private Color hoverColor;
    private Color startColor;

    private new Renderer renderer;

    private bool isBuildable = false;
    private bool isBuilt = false;
    private Tower towerOn = null;

    public bool IsBuilt { get => isBuilt; set => isBuilt = value; }
    public bool IsBuildable { get => isBuildable; set => isBuildable = value; }
    public Tower TowerOn { get => towerOn; set => towerOn = value; }
    public Color HoverColor { get => hoverColor; set => hoverColor = value; }

    public virtual void Awake()
    {
        
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        renderer = GetComponent<Renderer>();
        startColor = renderer.material.color;
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }

    public virtual void OnMouseEnter()
    {
        //if (!eventSystem.IsPointerOverGameObject())
        renderer.material.color = (!MouseManager.Instance.IsMouseOverUI()) ? HoverColor : startColor;

    }

    public virtual void OnMouseExit()
    {
        renderer.material.color = startColor;
    }

    public virtual void OnMouseDown()
    {
        if (!MouseManager.Instance.IsMouseOverUI())
        {

            if (BuildMenu.Instance.IsMenuOpen)
                BuildMenu.Instance.CloseMenu();
        }
    }

    public virtual void OnMouseOver()
    {
        renderer.material.color = (!MouseManager.Instance.IsMouseOverUI()) ? HoverColor : startColor;
        //if (!MouseManager.Instance.IsMouseOverUI())
            //renderer.material.color = hoverColor;
        
    }
}
