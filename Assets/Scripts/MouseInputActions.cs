// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/MouseInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @MouseInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @MouseInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""MouseInputActions"",
    ""maps"": [
        {
            ""name"": ""Click"",
            ""id"": ""112be68b-5814-4c79-9b3a-f241a7a5810c"",
            ""actions"": [
                {
                    ""name"": ""Left Button"",
                    ""type"": ""Button"",
                    ""id"": ""243aeb34-0034-4093-9505-570f19115dc1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""0b638b39-e0ab-4af7-9002-2ac67356c707"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Mouse"",
                    ""action"": ""Left Button"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Position"",
            ""id"": ""f51cdcae-32ed-43d3-ba76-b9bf5551328b"",
            ""actions"": [
                {
                    ""name"": ""Position"",
                    ""type"": ""Value"",
                    ""id"": ""c3616ffe-7123-44d1-b843-bc2c6a8e6a98"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""82a050b4-a714-4ce7-8f6d-56e024995f68"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Mouse"",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Mouse"",
            ""bindingGroup"": ""Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Click
        m_Click = asset.FindActionMap("Click", throwIfNotFound: true);
        m_Click_LeftButton = m_Click.FindAction("Left Button", throwIfNotFound: true);
        // Position
        m_Position = asset.FindActionMap("Position", throwIfNotFound: true);
        m_Position_Position = m_Position.FindAction("Position", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Click
    private readonly InputActionMap m_Click;
    private IClickActions m_ClickActionsCallbackInterface;
    private readonly InputAction m_Click_LeftButton;
    public struct ClickActions
    {
        private @MouseInputActions m_Wrapper;
        public ClickActions(@MouseInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @LeftButton => m_Wrapper.m_Click_LeftButton;
        public InputActionMap Get() { return m_Wrapper.m_Click; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ClickActions set) { return set.Get(); }
        public void SetCallbacks(IClickActions instance)
        {
            if (m_Wrapper.m_ClickActionsCallbackInterface != null)
            {
                @LeftButton.started -= m_Wrapper.m_ClickActionsCallbackInterface.OnLeftButton;
                @LeftButton.performed -= m_Wrapper.m_ClickActionsCallbackInterface.OnLeftButton;
                @LeftButton.canceled -= m_Wrapper.m_ClickActionsCallbackInterface.OnLeftButton;
            }
            m_Wrapper.m_ClickActionsCallbackInterface = instance;
            if (instance != null)
            {
                @LeftButton.started += instance.OnLeftButton;
                @LeftButton.performed += instance.OnLeftButton;
                @LeftButton.canceled += instance.OnLeftButton;
            }
        }
    }
    public ClickActions @Click => new ClickActions(this);

    // Position
    private readonly InputActionMap m_Position;
    private IPositionActions m_PositionActionsCallbackInterface;
    private readonly InputAction m_Position_Position;
    public struct PositionActions
    {
        private @MouseInputActions m_Wrapper;
        public PositionActions(@MouseInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Position => m_Wrapper.m_Position_Position;
        public InputActionMap Get() { return m_Wrapper.m_Position; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PositionActions set) { return set.Get(); }
        public void SetCallbacks(IPositionActions instance)
        {
            if (m_Wrapper.m_PositionActionsCallbackInterface != null)
            {
                @Position.started -= m_Wrapper.m_PositionActionsCallbackInterface.OnPosition;
                @Position.performed -= m_Wrapper.m_PositionActionsCallbackInterface.OnPosition;
                @Position.canceled -= m_Wrapper.m_PositionActionsCallbackInterface.OnPosition;
            }
            m_Wrapper.m_PositionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Position.started += instance.OnPosition;
                @Position.performed += instance.OnPosition;
                @Position.canceled += instance.OnPosition;
            }
        }
    }
    public PositionActions @Position => new PositionActions(this);
    private int m_MouseSchemeIndex = -1;
    public InputControlScheme MouseScheme
    {
        get
        {
            if (m_MouseSchemeIndex == -1) m_MouseSchemeIndex = asset.FindControlSchemeIndex("Mouse");
            return asset.controlSchemes[m_MouseSchemeIndex];
        }
    }
    public interface IClickActions
    {
        void OnLeftButton(InputAction.CallbackContext context);
    }
    public interface IPositionActions
    {
        void OnPosition(InputAction.CallbackContext context);
    }
}
