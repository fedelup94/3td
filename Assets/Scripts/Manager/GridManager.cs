﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{

    [SerializeField]
    private Color hoverColor;
    private Color startColor;

    private MouseInputActions inputActions;

    //private Renderer rend;
    private Grid grid;


    private void Awake()
    {
        inputActions = new MouseInputActions();
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        //rend = GetComponent<Renderer>();
        //startColor = rend.material.color;
        grid = new Grid(30, 30, 4, transform.position);
        grid.DrawLines();
    }

    // Update is called once per frame
    void Update()
    {
        if (inputActions.Click.LeftButton.triggered)
        {
            grid.SetValue(GetMouseWorldPosition(), 10);
        }
    }

    //private void OnMouseEnter()
    //{
    //    rend.material.color = hoverColor;
    //}

    //private void OnMouseExit()
    //{
    //    rend.material.color = startColor;
    //}

    //private void OnDrawGizmosSelected()
    //{
    //    grid.DrawLines();
    //}


    public static TextMesh CreateWorldText(string text, Transform parent = null, Vector3 localPosition = default(Vector3), int fontSize = 40, Color? color = null, TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Left, int sortingOrder = 5000)
    {
        color = (color == null) ? Color.white : color;
        return CreateWorldText(parent, text, localPosition, fontSize, (Color)color, textAnchor, textAlignment, sortingOrder);
    }

    public static TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, TextAnchor textAnchor, TextAlignment textAlignment, int sortingOrder)
    {
        GameObject gameObject = new GameObject("World Text", typeof(TextMesh));
        Transform transform = gameObject.transform;
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();

        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;

        // Rotate on X
        var angle = transform.rotation.eulerAngles;
        angle.x = 90;

        transform.rotation = Quaternion.Euler(angle);
        return textMesh;
    }

    public Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePos = new Vector3(inputActions.Position.Position.ReadValue<Vector2>().x, inputActions.Position.Position.ReadValue<Vector2>().y);
        Debug.Log("X: " + mousePos.x + "   Y: " + mousePos.y);
        Vector3 vec = GetMouseWorldPositionWithZ(mousePos, Camera.main);
        Debug.Log(vec);

        //inputActions.Position.Position.
        //Camera.main.ScreenToWorldPoint(inputActions.Position.Position.)
        vec.z = 0f;
        return vec;
    }

    public Vector3 GetMouseWorldPositionWithZ(Camera worldCamera)
    {
        return GetMouseWorldPositionWithZ(inputActions.Position.Position.ReadValue<Vector3>(), worldCamera);
    }

    public Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera worldCamera)
    {
        Vector3 worldPosition = worldCamera.ScreenToWorldPoint(screenPosition);
        return worldPosition;
    }


}
