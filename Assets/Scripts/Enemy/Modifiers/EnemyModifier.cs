﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Modifier : ScriptableObject
{
    //For Generics porpouse
    //public abstract void OnStartResolveModifier();
    //public abstract void OnUpdateResolveModifier();
    //public abstract void OnDamageTakenResolveModifier();
    //public abstract void AfterDamageTakenResolveModifier();
    //public abstract void OnDeathResolveModifier();
}

public abstract class EnemyModifier<T> : Modifier, IEnemyModifier<T>
    where T : Component
{
    public abstract void OnStartResolveModifier(T target);
    public abstract void OnUpdateResolveModifier(T target);
    public abstract void BeforeDamageTakenResolveModifier(T target);
    public abstract void AfterDamageTakenResolveModifier(T target);
    public abstract void OnDeathResolveModifier(T target);
}
