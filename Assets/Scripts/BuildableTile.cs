﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildableTile : Tile
{

    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        IsBuildable = true;

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void OnMouseEnter()
    {
        base.OnMouseEnter();
    }

    public override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    public override void OnMouseDown()
    {
        //base.OnMouseDown();
        //if built                    -->   open tower upgrade menu
        //if not built and buildable  -->   open construction menu
        if (!MouseManager.Instance.IsMouseOverUI())
        {
            if (IsBuildable)
            {
                if (!IsBuilt)
                {
                    HandleBuildMenu();
                    
                    //var tower = Instantiate(GameAssets.Instance.ArrowTower, transform.position, new Quaternion(), transform);
                    //tower.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y + 5, tower.transform.position.z);
                    //TowerOn = tower.GetComponent<ArrowTower>();
                    //IsBuilt = true;
                    Debug.Log("AE");

                }
                else
                    Debug.Log("NOT AE");

            }
        }
            


    }

    private void HandleBuildMenu()
    {
        if (BuildMenu.Instance.IsMenuOpen)
            BuildMenu.Instance.CloseMenu();
        else
            BuildMenu.Instance.SpawnMenu(transform.position, this);
    }
}
