﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourceManager : SingletonPersistent<ResourceManager>
{

    [SerializeField]
    TextMeshProUGUI moneyText;

    [SerializeField]
    TextMeshProUGUI ironText;

    [SerializeField]
    TextMeshProUGUI scienceText;

    private Resource money;
    private Resource iron;
    private Resource science;

    public Resource Money { get => money; set => money = value; }
    public Resource Iron { get => iron; set => iron = value; }
    public Resource Science { get => science; set => science = value; }
    public TextMeshProUGUI MoneyText { get => moneyText; set => moneyText = value; }
    public TextMeshProUGUI IronText { get => ironText; set => ironText = value; }
    public TextMeshProUGUI ScienceText { get => scienceText; set => scienceText = value; }

    public override void Awake()
    {
        base.Awake();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();
        InitResources();

    }

    private void InitResources()
    {
        Money = new Resource(MoneyText, Resource_Type.MONEY, 250);
        Iron = new Resource(IronText, Resource_Type.IRON, 100);
        Science = new Resource(ScienceText, Resource_Type.SCIENCE, 10);
        UpdateResourceText(Money);
        UpdateResourceText(Iron);
        UpdateResourceText(Science);
    }

    public override void Update()
    {
        base.Update();
    }

    public bool UseResource(Resource_Type type, uint cost)
    {
        switch (type)
        {
            case Resource_Type.MONEY:
                return UseResource(Money, cost);
            case Resource_Type.IRON:
                return UseResource(Iron, cost);
            case Resource_Type.SCIENCE:
                return UseResource(Science, cost);
            default:
                return false;
        }
    }

    public bool UseResource(Resource resource, uint cost)
    {
        if (resource.Quantity >= cost)
        {
            resource.Quantity -= cost;
            UpdateResourceText(resource);
            return true;
        }
        return false;
    }

    public bool IsResourceEnough(Resource_Type type, uint cost)
    {
        switch (type)
        {
            case Resource_Type.MONEY:
                return IsResourceEnough(Money, cost);
            case Resource_Type.IRON:
                return IsResourceEnough(Iron, cost);
            case Resource_Type.SCIENCE:
                return IsResourceEnough(Science, cost);
            default:
                return false;
        }
    }

    public bool IsResourceEnough(Resource resource, uint cost)
    {
        return (resource.Quantity >= cost);
    }

    public void UpdateResourceText(Resource_Type type)
    {
        switch (type)
        {
            case Resource_Type.MONEY:
                UpdateResourceText(Money);
                break;
            case Resource_Type.IRON:
                UpdateResourceText(Iron);
                break;
            case Resource_Type.SCIENCE:
                UpdateResourceText(Science);
                break;
            default:
                break;
        }
    }

    public void UpdateResourceText(Resource resource)
    {
        UpdateResourceTextWithText(resource, resource.Quantity.ToString());
    }

    public void UpdateResourceTextWithText(Resource resource, string newText)
    {
        resource.ResourceText.text = newText;
    }

    //public void UpdateMoneyText()
    //{
    //    MoneyText.text = Money.ToString();
    //}

    //public void UpdateIronText()
    //{
    //    IronText.text = Iron.ToString();
    //}

    //public void UpdateScienceText()
    //{
    //    ScienceText.text = Science.ToString();
    //}
}


public enum Resource_Type { MONEY, IRON, SCIENCE };

//Not needed but may be useful for future ?
public class Resource
{
    Resource_Type resource_Type;
    TextMeshProUGUI resourceText;
    private uint quantity;

    public Resource(TextMeshProUGUI text, Resource_Type type) { Quantity = 0; ResourceText = text; Resource_Type = type; }
    public Resource(TextMeshProUGUI text, Resource_Type type, uint _quantity) { Quantity = _quantity; ResourceText = text; Resource_Type = type; }

    public uint Quantity { get => quantity; set => quantity = value; }
    public TextMeshProUGUI ResourceText { get => resourceText; set => resourceText = value; }
    public Resource_Type Resource_Type { get => resource_Type; set => resource_Type = value; }
}
