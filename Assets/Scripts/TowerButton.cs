﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class TowerButton : MonoBehaviour
{

    [SerializeField]
    private TOWER_TYPE type;

    private GraphicRaycaster raycaster;

    public TOWER_TYPE Type { get => type; set => type = value; }
    public GraphicRaycaster Raycaster { get => raycaster; set => raycaster = value; }

    private void Awake()
    {
        raycaster = GetComponent<GraphicRaycaster>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            PointerEventData data = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            data.position = Input.mousePosition;
            Raycaster.Raycast(data, results);
            foreach (RaycastResult result in results)
            {
                Debug.Log("Hit " + result.gameObject.name);
                BuildMenu.Instance.BuildTower(Type);
            }
            //Debug.Log(type.ToString());

        }
    }



    private void OnMouseDown()
    {




    }


}
