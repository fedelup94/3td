﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor", menuName = "CustomSO/Enemy/Modifier/ArmorModifier")]
public class ArmorModifier : EnemyModifier<Enemy>
{

    [SerializeField]
    private uint armor;

    public uint Armor { get => armor; set => armor = value; }


    public override void BeforeDamageTakenResolveModifier(Enemy target)
    {

    }

    public override void AfterDamageTakenResolveModifier(Enemy target)
    {

    }

    public override void OnDeathResolveModifier(Enemy target)
    {

    }

    public override void OnStartResolveModifier(Enemy target)
    {
        target.Armor += Armor;
    }

    public override void OnUpdateResolveModifier(Enemy target)
    {
    }
}
