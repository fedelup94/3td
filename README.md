# 3TD

Idee:

    - Singleplayer
    - Multiplayer Massivo ? (20-100 px.)


Risorse:
    - Oro/Soldi (per acquistare torrette e upgrade)
    - Ferro (per edifici specifici e muro per bloccare le strade)
    - Gemme (per potenziamenti elementali) (Fuoco / Acqua / Oscurità / Luce / Veleno / Terra)
    - Punti Scienza (per ricerca)
    - XP (per livelli)



Torri:

    - DAMAGE
        - Standard 
        - Cannon
        - Missile (contro volanti)
        - Time-Shift (fa tornare indietro i creep)
        - Tesla (elettrica)
        - Laser (contro armatura)
        - Sniper (high dmg, high range, low speed)
    
    - NO DAMAGE
        - Wall (un path da ogni spawn point alla fine deve rimanere sempre possibile)
        - Aura (effetti passivi)
        - Bank (guadagno maggiore per wave o interessi)
        - Science (permette ricerca, ed effetti vari come preview della/e prossima/e wave, più danno contro determinate unità)

Gameplay:
    
    A fine wave soldi + xp 

    con XP level system con abilità attive/passive (o solo passive)

    torri a quando potenziate hanno socket per le gemme per potenziamenti 
    unici in base alla torre (ottengono elemento che cambia stats)


    Singleplayer
    TD normale con talenti e potenziamenti passivi
    (aggiungere carte potere/potenziamento?)

    //old idea
    Multiplayer
    - Diviso in 2 fasi (round)
    1: raccolta delle risorse (da capire come)
    2: TD ad ondate con difesa da nemici (come renderlo asincrono?)
    

    //new idea
    Multiplayer
    - Diviso in 3 momenti
    1: costruzione
    2: wave
    3: eventi

    eventi divisi in
    - positivi per giocatore
    - negativi per avversari




Prima Versione dell'HUD:

![Idea_HUD](/uploads/de4732860c5ccaf940e3c47775464fb6/Idea_HUD.png)


    

