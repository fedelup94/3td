﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyModifier<T>
    where T : Component
{
    //private ENEMY_MODIFIERS_TYPE type;
    void OnStartResolveModifier(T target);
    void OnUpdateResolveModifier(T target);
    void BeforeDamageTakenResolveModifier(T target);
    void AfterDamageTakenResolveModifier(T target);
    void OnDeathResolveModifier(T target);

}
