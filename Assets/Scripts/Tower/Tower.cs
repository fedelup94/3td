﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class Tower : MonoBehaviour
{
    private uint cost;

    public abstract uint Cost { get; set; }

    public virtual void Awake()
    {

    }

    // Start is called before the first frame update
    public virtual void Start()
    {

    }

    // Update is called once per frame
    public virtual void Update()
    {

    }

    public virtual void LateUpdate()
    {

    }

    public virtual void FixedUpdate()
    {

    }


    //CREA METODO CHE PRENDE DAMAGETOWERTYPE E INIZIALIZZA


}
