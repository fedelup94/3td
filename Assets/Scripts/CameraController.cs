﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{


    [SerializeField]
    private float speed;

    [SerializeField]
    private float fovMax = 60;

    [SerializeField]
    private float fovMin = 30;

    [SerializeField]
    private float transitionTime = 3;

    [SerializeField]
    private float fovStep = 5;

    private float fov;
    private float lerpTime;

    private CameraControls inputActions;

    public float Speed { get => speed; set => speed = value; }
    public float FovMax { get => fovMax; set => fovMax = value; }
    public float FovMin { get => fovMin; set => fovMin = value; }
    public float TransitionTime { get => transitionTime; set => transitionTime = value; }
    public float FovStep { get => fovStep; set => fovStep = value; }

    private void Awake()
    {
        inputActions = new CameraControls();
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        fov = 60;
    }

    // Update is called once per frame
    void Update()
    {

        HandleMovement();
        HandleZoom();

    }

    private void LateUpdate()
    {
    }

    void HandleZoom()
    {
        var wheel = inputActions.Move.Zoom.ReadValue<Vector2>();

        if (wheel.x != 0 || wheel.y != 0) { }
        //Debug.Log(wheel);

        if (wheel.y > 0)
            ZoomIn();
        else if (wheel.y < 0)
            ZoomOut();

        Camera.main.fieldOfView = fov;



    }


    void ZoomIn()
    {
        lerpTime += Time.deltaTime;

        var t = lerpTime / TransitionTime;

        t = Mathf.SmoothStep(0, 1, t);
        fov = Mathf.Lerp(fov - FovStep, FovMin, t);
        fov = Mathf.Clamp(fov, FovMin, FovMax);
    }

    void ZoomOut()
    {
        lerpTime += Time.deltaTime;

        var t = lerpTime / TransitionTime;

        t = Mathf.SmoothStep(0, 1, t);
        fov = Mathf.Lerp(fov + FovStep, FovMax, t);
        fov = Mathf.Clamp(fov, FovMin, FovMax);

    }

    void HandleMovement()
    {
        float forward = inputActions.Move.Forward.ReadValue<float>();
        float side = -inputActions.Move.Left.ReadValue<float>();

        Vector3 pos = transform.position;

        pos.z += forward * Speed * Time.deltaTime;
        pos.x += side * Speed * Time.deltaTime;

        transform.position = pos;
    }
}
