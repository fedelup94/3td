﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TargetPriority { Strongest, Weakest, Boss_First, Groups }; //Groups TODO

public abstract class DamageTower : Tower
{

    [SerializeField]
    private DamageTowerType towerType;

    private float damage;
    private float attacksPerSecond;
    private float splash;
    private float splashFalloff;
    private float range;
    private bool canTargetAir;
    private bool isUpgradable;
    private GameObject upgrade;
    private List<Enemy> target;
    private uint numberOfTarget;
    private float projectileSpeed;
    private float attackSpeedTimer;
    private TargetPriority priority;

    private bool hasCoroutineStarted;

    public float Damage { get => damage; set => damage = value; }
    public float Range { get => range; set => range = value; }
    public bool CanTargetAir { get => canTargetAir; set => canTargetAir = value; }
    public List<Enemy> Target { get => target; set => target = value; }
    public bool IsUpgradable { get => isUpgradable; set => isUpgradable = value; }
    public float Splash { get => splash; private set => splash = value; }
    public float AttacksPerSecond { get => attacksPerSecond; set => attacksPerSecond = value; }
    public GameObject Upgrade { get => upgrade; set => upgrade = value; }
    public DamageTowerType TowerType { get => towerType; set => towerType = value; }
    public uint NumberOfTarget { get => numberOfTarget; set => numberOfTarget = value; }
    public float ProjectileSpeed { get => projectileSpeed; set => projectileSpeed = value; }
    public float AttackSpeedTimer { get => attackSpeedTimer; set => attackSpeedTimer = value; }
    public float SplashFalloff { get => splashFalloff; set => splashFalloff = value; }
    public TargetPriority Priority { get => priority; set => priority = value; }


    public override uint Cost { get => TowerType.Cost; set => TowerType.Cost = value; }

    public abstract Projectile GetPooledProjectile();


    public override void Awake()
    {
        base.Awake();
        hasCoroutineStarted = false;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();
        InitTower();
    }

    public override void Update()
    {
        base.Update();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.localPosition, TowerType.Range);
    }

    private void InitTower()
    {
        if (TowerType != null)
        {
            Damage = TowerType.Damage;
            Range = TowerType.Range;
            CanTargetAir = TowerType.CanTargetAir;
            Target = null;
            IsUpgradable = (TowerType.Upgrade != null);
            Splash = TowerType.Splash;
            AttacksPerSecond = TowerType.AttacksPerSecond;
            Upgrade = TowerType.Upgrade;
            NumberOfTarget = TowerType.NumberOfTargets;
            Target = new List<Enemy>();
            ProjectileSpeed = TowerType.ProjectileSpeed;
            Cost = TowerType.Cost;
        }
    }

    public virtual void FindTarget()
    {

        if (Target.Count < NumberOfTarget)
        {

            var colliders = Physics.OverlapSphere(transform.position, Range, LayerMask.GetMask("Enemy"));

            List<Enemy> allEnemies = new List<Enemy>();

            for (int i = 0; i < colliders.Length; ++i)            
                allEnemies.Add(colliders[i].GetComponent<Enemy>());

            //Remove all untargetable
            CleanUntargetables(allEnemies);
            //Order By Priority
            SortTargets(allEnemies);

            for (int i = 0; Target.Count < NumberOfTarget && i < allEnemies.Count; ++i)
            {
                if (!Target.Contains(allEnemies[i]))
                    Target.Add(allEnemies[i]);
            }


        }
    }

    private List<Enemy> CleanUntargetables(List<Enemy> allEnemies)
    {
        foreach(Enemy enemy in allEnemies.ToArray())
        {
            if (!CanTargetAir)
                if (enemy.IsAirUnit)
                    allEnemies.Remove(enemy);
        }

        return allEnemies;
    }

    public List<Enemy> SortTargets(List<Enemy> targets)
    {
        switch (Priority)
        {
            case TargetPriority.Strongest:
                targets.Sort(new EnemyComparer_StrongestFirst());
                break;
            case TargetPriority.Weakest:
                targets.Sort(new EnemyComparer_WeakestFirst());
                break;
            case TargetPriority.Boss_First:
                targets.Sort(new EnemyComparer_BossFirst());
                break;
            case TargetPriority.Groups:
                targets.Sort(new EnemyComparer_GroupsFirst());
                break;
        }

        return targets;

    }

    public IEnumerator DestroyTarget(Enemy target)
    {
        if (target == null)
            Target.Remove(target);
        yield break;
    }


    public virtual void CleanTarget()
    {
        foreach (Enemy _enemy in Target.ToArray())
        {
            if (_enemy == null)
                Target.Remove(_enemy);
            else if (Vector3.Distance(_enemy.transform.position, transform.position) > Range)
                Target.Remove(_enemy);

        }
    }



    public virtual void Attack()
    {
        if (AttackSpeedTimer <= 0)
        {
            AttackSpeedTimer = 1 / AttacksPerSecond;
            foreach (Enemy _enemy in Target)
            {

                //var proj = ObjectPooler.Instance.GetPooledStandardProjectile();
                var proj = GetPooledProjectile();
                proj.ShootProjectile(transform, _enemy, TowerType);
            }

        }
        else
            StartCoroutine(AttackCooldown());

    }

    public virtual IEnumerator AttackCooldown()
    {
        if (!hasCoroutineStarted)
        {
            hasCoroutineStarted = true;
            while (AttackSpeedTimer > 0)
            {
                yield return new WaitForSeconds(Time.deltaTime);
                AttackSpeedTimer -= Time.deltaTime;
            }
            hasCoroutineStarted = false;
        }
        yield break;
    }

    public void SetNextPriority()
    {
        switch (Priority)
        {
            case TargetPriority.Strongest:
                Priority = TargetPriority.Weakest;
                break;
            case TargetPriority.Weakest:
                Priority = TargetPriority.Boss_First;
                break;
            case TargetPriority.Boss_First:
                Priority = TargetPriority.Strongest;
                break;
            case TargetPriority.Groups:
                //TODO
                break;
        }
    }

    public void RotateTowardTarget()
    {
        if (Target.Count > 0)
            RotateTowardTarget(Target[0]);
    }

    public void RotateTowardTarget(Enemy target)
    {
        transform.LookAt(target.transform);
    }





}
