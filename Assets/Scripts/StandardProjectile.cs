﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardProjectile : Projectile
{
    public override void Awake()
    {
        base.Awake();
    }



    public override void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats)
    {
        ShootProjectile(startingPosition, _target, towerStats, StandardTimerToDeath);
    }


    public override void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats, float timer)
    {
        base.ShootProjectile(startingPosition, _target, towerStats, timer);

    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void CalculateDamage()
    {
        TargetDamageable.TakeDamage(Damage);
    }
}
