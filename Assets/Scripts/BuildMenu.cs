﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMenu : Singleton<BuildMenu>
{

    private Animator animator;
    private bool isMenuOpen;
    TowerButton[] towerButtons = new TowerButton[4];

    private Tile referredTile;

    public Animator Animator { get => animator; set => animator = value; }
    public bool IsMenuOpen { get => isMenuOpen; set => isMenuOpen = value; }
    public TowerButton[] TowerButtons { get => towerButtons; set => towerButtons = value; }

    public override void Awake()
    {
        base.Awake();
        Animator = GetComponent<Animator>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }


    public void SpawnMenu(Vector3 position, Tile tile)
    {
        //transform.position = position;
        if (!IsMenuOpen)
        {
            transform.position = new Vector3(position.x - .5f, 10, position.z - .5f);
            IsMenuOpen = true;
            animator.SetBool("OpenMenu", IsMenuOpen);
            referredTile = tile;
        }
    }

    public void CloseMenu()
    {
        if (IsMenuOpen)
        {
            IsMenuOpen = false;
            animator.SetBool("OpenMenu", IsMenuOpen);
            referredTile = null;
        }
    }

    public void CloseMenuIstant()
    {
        transform.position = new Vector3(-999999, -999999, -999999);
        CloseMenu();
    }

    public void BuildTower(TOWER_TYPE type)
    {
        if (!referredTile.IsBuilt && referredTile.IsBuildable)
        {
            var tower = GameAssets.Instance.GetTowerFromAssets(type, referredTile.transform);
            Tower towerInfo = null;
            uint towerCost = 0;

            switch (type)
            {
                case TOWER_TYPE.ARROW:
                    towerInfo = tower.GetComponent<ArrowTower>();
                    break;
                case TOWER_TYPE.CANNON:
                    towerInfo = tower.GetComponent<CannonTower>();
                    break;
                case TOWER_TYPE.MISSILE:
                    break;
                case TOWER_TYPE.TIME_SHIFT:
                    break;
                case TOWER_TYPE.TESLA:
                    break;
                case TOWER_TYPE.LASER:
                    break;
                case TOWER_TYPE.SNIPER:
                    towerInfo = tower.GetComponent<SniperTower>();
                    break;
            }

            if (towerInfo != null)
            {
                towerCost = towerInfo.Cost;
                if (CheckPrice(Resource_Type.MONEY, towerCost))
                {
                    SpendOnResource(Resource_Type.MONEY, towerCost);
                    referredTile.TowerOn = towerInfo;
                    referredTile.IsBuilt = true;
                    CloseMenuIstant();
                }

            }

            //switch (type)
            //{
            //    case TOWER_TYPE.ARROW:

            //        break;
            //    case TOWER_TYPE.CANNON:

            //        break;
            //    case TOWER_TYPE.MISSILE:
            //        break;
            //    case TOWER_TYPE.TIME_SHIFT:
            //        break;
            //    case TOWER_TYPE.TESLA:
            //        break;
            //    case TOWER_TYPE.LASER:
            //        break;
            //    case TOWER_TYPE.SNIPER:
            //        //towerInfo = tower.GetComponent<SniperTower>();
            //        //referredTile.TowerOn = tower.GetComponent<SniperTower>();
            //        break;
            //}


            //if (CheckPrice(Resource_Type.MONEY, towerInfo.TowerType))
            //{
            //    SpendOnResource(Resource_Type.MONEY, towerInfo.Cost);
            //    referredTile.TowerOn = towerInfo;
            //    referredTile.IsBuilt = true;
            //    CloseMenuIstant();
            //}



            //if (CheckPrice(Resource_Type.MONEY, 100))
            //{
            //    SpendOnResource(Resource_Type.MONEY, 100);

            //    switch (type)
            //    {
            //        case TOWER_TYPE.ARROW:
            //            referredTile.TowerOn = tower.GetComponent<ArrowTower>();
            //            break;
            //        case TOWER_TYPE.CANNON:
            //            referredTile.TowerOn = tower.GetComponent<CannonTower>();
            //            break;
            //        case TOWER_TYPE.MISSILE:
            //            break;
            //        case TOWER_TYPE.TIME_SHIFT:
            //            break;
            //        case TOWER_TYPE.TESLA:
            //            break;
            //        case TOWER_TYPE.LASER:
            //            break;
            //        case TOWER_TYPE.SNIPER:
            //            referredTile.TowerOn = tower.GetComponent<SniperTower>();
            //            break;
            //    }
            //    referredTile.IsBuilt = true;
            //    CloseMenuIstant();
            //}
            //var tower = Instantiate(Utils.GetTowerPrefabFromType(type), referredTile.transform.position, new Quaternion(), referredTile.transform);
            //tower.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y + 5, tower.transform.position.z);

        }
    }


    private bool CheckPrice(Resource_Type type, uint cost)
    {
        return ResourceManager.Instance.IsResourceEnough(type, cost);
    }

    private void SpendOnResource(Resource_Type type, uint cost)
    {
        ResourceManager.Instance.UseResource(type, cost);
    }

}
