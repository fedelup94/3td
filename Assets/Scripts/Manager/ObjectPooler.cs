﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : SingletonPersistent<ObjectPooler>
{

    [Header("Standard Projectile")]
    [SerializeField]
    private GameObject standardProjectileToPool;
    [SerializeField]
    private int amountStandardProjectileToPool;
    private List<StandardProjectile> pooledStandardProjectiles = new List<StandardProjectile>();

    [Header("Splash Projectile")]
    [SerializeField]
    private GameObject splashProjectileToPool;
    [SerializeField]
    private int amountSplashProjectileToPool;
    private List<SplashProjectile> pooledSplashProjectiles = new List<SplashProjectile>();

    [Header("Floating Text")]
    [SerializeField]
    private GameObject floatingTextToPool;
    [SerializeField]
    private int amountFloatingTextToPool;
    private List<TextMesh> pooledFloatingTexts = new List<TextMesh>();

    public override void Awake()
    {
        base.Awake();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();
        StartCoroutine(InstantiateProjectiles());
    }

    public override void Update()
    {
        base.Update();
    }


    public StandardProjectile GetPooledStandardProjectile()
    {
        for (int i = 0; i < pooledStandardProjectiles.Count; ++i)
        {
            if (!pooledStandardProjectiles[i].gameObject.activeInHierarchy)
                return pooledStandardProjectiles[i];
        }
        // should check for errors?
        return null;
    }

    public SplashProjectile GetPooledSplashProjectile()
    {
        for (int i = 0; i < pooledSplashProjectiles.Count; ++i)
        {
            if (!pooledSplashProjectiles[i].gameObject.activeInHierarchy)
                return pooledSplashProjectiles[i];
        }
        // should check for errors?
        return null;
    }

    public TextMesh GetPooledFloatingText()
    {
        for (int i = 0; i < pooledFloatingTexts.Count; ++i)
        {
            if (!pooledFloatingTexts[i].gameObject.activeInHierarchy)
                return pooledFloatingTexts[i];
        }
        // should check for errors?
        return null;
    }

    private IEnumerator InstantiateProjectiles()
    {
        const int k = 4;

        while (pooledStandardProjectiles.Count < amountStandardProjectileToPool)
        {
            for (int i = 0; i < amountStandardProjectileToPool; i += k)
            {
                for (int y = 0; y < k; ++y)
                {
                    pooledStandardProjectiles.Add(InstantiateObjectToPool<StandardProjectile>(standardProjectileToPool));
                }
            }
            yield return new WaitForEndOfFrame();
        }

        while (pooledSplashProjectiles.Count < amountSplashProjectileToPool)
        {
            for (int i = 0; i < amountSplashProjectileToPool; i += k)
            {
                for (int y = 0; y < k; ++y)
                {
                    pooledSplashProjectiles.Add(InstantiateObjectToPool<SplashProjectile>(splashProjectileToPool));
                }
            }
            yield return new WaitForEndOfFrame();
        }

        while (pooledFloatingTexts.Count < amountFloatingTextToPool)
        {
            for (int i = 0; i < amountFloatingTextToPool; i += k)
            {
                for (int y = 0; y < k; ++y)
                {
                    pooledFloatingTexts.Add(InstantiateObjectToPool<TextMesh>(floatingTextToPool));
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private T InstantiateObjectToPool<T>(GameObject obj)
        where T : Component
    {
        T pooled = Instantiate(obj).GetComponent<T>();
        pooled.gameObject.SetActive(false);
        pooled.transform.parent = this.gameObject.transform;
 
        return pooled;
    }


}
