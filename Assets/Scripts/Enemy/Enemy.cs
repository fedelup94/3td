﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public enum ENEMY_TYPE { Normal, Boss };
//public enum ENEMY_MODIFIERS_TYPE { AIR, ARMOR, AURA_ARMOR }; // more modifiers to come

public class Enemy : MonoBehaviour, IDamageable
{

    [SerializeField]
    private EnemyStat stats;

    private float hp;
    private float speed;
    private uint armor;
    private bool isAirUnit;

    [SerializeField]
    private Transform end_Waypoint;     // To Be Moved Useless to save in all enemies

    //[SerializeField]
    //private Transform floatingTextTransform;

    //private Animator floatingTextAnimator;
    //private TextMesh floatingText;
    private NavMeshAgent navMeshAgent;

    public float HP { get => hp; set => hp = value; }
    public float Speed { get => speed; set => speed = value; }
    public NavMeshAgent NavMeshAgent { get => navMeshAgent; set => navMeshAgent = value; }
    public uint Armor { get => armor; set => armor = value; }
    public EnemyStat Stats { get => stats; set => stats = value; }
    public bool IsAirUnit { get => isAirUnit; set => isAirUnit = value; }





    // Start is called before the first frame update
    void Start()
    {

        InitStats();
        NavMeshAgent = GetComponent<NavMeshAgent>();
        NavMeshAgent.SetDestination(end_Waypoint.transform.position);
        NavMeshAgent.isStopped = false;
        //floatingText = floatingTextTransform.GetComponent<TextMesh>();
        //floatingTextAnimator = floatingTextTransform.GetComponent<Animator>();

        CalculateModifiersOnStart();

    }

    private void InitStats()
    {
        HP = Stats.StartingHP;
        Speed = Stats.StartingSpeed;
        Armor = Stats.Armor;

    }

    // Update is called once per frame
    void Update()
    {
        CalculateModifiersOnUpdate();
    }

    public bool HasDied()
    {
        return (HP <= 0);
    }

    public void TakeDamage(float damage)
    {
        CalculateModifiersBeforeDamageTaken();

        HP -= (Mathf.Abs(damage) - Armor <= 0) ? 1 : Mathf.Abs(damage) - Armor;

        //HP -= Mathf.Abs(damage);

        //Should calculate before death or not?
        CalculateModifiersAfterDamageTaken();

        //SpawnFloatingText(damage.ToString());
        CheckDeath();
    }

    private void SpawnFloatingText(string text)
    {
        SpawnFloatingText(text, Color.white);
    }

    private void SpawnFloatingText(string text, Color color)
    {
        var floatingText = ObjectPooler.Instance.GetPooledFloatingText();

        floatingText.transform.position = gameObject.transform.position;

        var angle = floatingText.transform.rotation.eulerAngles;
        angle.x = 60;

        floatingText.text = text;

        floatingText.transform.rotation = Quaternion.Euler(angle);
        floatingText.color = color;
        floatingText.gameObject.SetActive(true);
        StartCoroutine(DisableFloatingText(floatingText, 2f));

        //floatingText.text = text;
        //floatingText.color = color;
        //floatingText.gameObject.SetActive(true);
        //floatingTextAnimator.SetTrigger("SpawnFloatingText");
        //StartCoroutine(DisableFloatingText(floatingText, 1f));
        //var angle = floatingTextTransform.parent.rotation.eulerAngles;
        //angle.x = 90;
        //floatingTextTransform.parent.rotation = Quaternion.Euler(angle);


        /*
         * 
         *         
         *         
         
        Transform transform = gameObject.transform;
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();

        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;

        // Rotate on X
        var angle = transform.rotation.eulerAngles;
        angle.x = 90;

        transform.rotation = Quaternion.Euler(angle);
        return textMesh;
         * 
         * 
         */


    }

    private IEnumerator DisableFloatingText(TextMesh floatingText, float timer)
    {
        while (timer > 0)
        {
            floatingText.transform.LookAt(Camera.main.transform);
            yield return new WaitForSeconds(Time.deltaTime);
            timer -= Time.deltaTime;
        }
        floatingText.gameObject.SetActive(false);

    }

    private void CheckDeath()
    {
        if (HasDied())
        {
            CalculateModifiersOnDeath();
            Destroy(gameObject);
        }
    }

    virtual public void CalculateModifiersOnStart()
    {
        foreach (var mod in Stats.Modifiers)        
            mod.OnStartResolveModifier(this);        
    }

    virtual public void CalculateModifiersOnUpdate()
    {
        foreach (var mod in Stats.Modifiers)
            mod.OnUpdateResolveModifier(this);        
    }

    virtual public void CalculateModifiersOnDeath()
    {
        foreach (var mod in Stats.Modifiers)
            mod.OnDeathResolveModifier(this);        
    }

    virtual public void CalculateModifiersBeforeDamageTaken()
    {
        foreach (var mod in Stats.Modifiers)        
            mod.BeforeDamageTakenResolveModifier(this);        
    }

    virtual public void CalculateModifiersAfterDamageTaken()
    {
        foreach (var mod in Stats.Modifiers)
            mod.AfterDamageTakenResolveModifier(this);
    }





}























public class EnemyComparer_WeakestFirst : IComparer<Enemy>
{
    public int Compare(Enemy x, Enemy y)
    {
        if (x.HP == y.HP)
            return 0;
        return (x.HP < y.HP) ? -1 : +1;
    }
}

public class EnemyComparer_StrongestFirst : IComparer<Enemy>
{
    public int Compare(Enemy x, Enemy y)
    {
        if (x.HP == y.HP)
            return 0;
        return (x.HP > y.HP) ? -1 : +1;
    }
}

public class EnemyComparer_BossFirst : IComparer<Enemy>
{
    public int Compare(Enemy x, Enemy y)
    {
        //TODO
        return 1;
    }
}

//public class EnemyComparer_GroundFirst : IComparer<Enemy>
//{
//    public int Compare(Enemy x, Enemy y)
//    {
//        if (x.IsAirUnit == y.IsAirUnit)
//            return 0;
//        if (!x.IsAirUnit)
//            return -1;
//        return 1;
//    }
//}

//public class EnemyComparer_AirFirst : IComparer<Enemy>
//{
//    public int Compare(Enemy x, Enemy y)
//    {
//        if (x.IsAirUnit == y.IsAirUnit)
//            return 0;
//        if (x.IsAirUnit)
//            return -1;
//        return 1;
//    }
//}

// No idea
public class EnemyComparer_GroupsFirst : IComparer<Enemy>
{
    public int Compare(Enemy x, Enemy y)
    {
        //TODO
        return 1;
    }
}
