﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Enemy", menuName = "CustomSO/Enemy/EnemyStat")]
public class EnemyStat : ScriptableObject
{

    [SerializeField]
    private float startingHP;

    [SerializeField]
    private float startingSpeed;

    [SerializeField]
    private uint armor;

    [SerializeField]
    private ENEMY_TYPE type;

    [SerializeField]
    List<EnemyModifier<Enemy>> modifiers;

    public float StartingHP { get => startingHP; set => startingHP = value; }
    public float StartingSpeed { get => startingSpeed; set => startingSpeed = value; }
    public uint Armor { get => armor; set => armor = value; }
    public ENEMY_TYPE Type { get => type; set => type = value; }
    public List<EnemyModifier<Enemy>> Modifiers { get => modifiers; set => modifiers = value; }
}
