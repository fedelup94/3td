﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{

    private Enemy enemyTarget;
    private float damage;
    private float timerToDeath;
    private float speed;
    private static readonly float standardTimerToDeath = 3f;

    private Vector3 moveDir;

    public float TimerToDeath { get => timerToDeath; set => timerToDeath = value; }
    public float Speed { get => speed; set => speed = value; }
    public Enemy TargetDamageable { get => enemyTarget; set => enemyTarget = value; }
    public float Damage { get => damage; set => damage = value; }
    public Vector3 MoveDir { get => moveDir; set => moveDir = value; }

    public static float StandardTimerToDeath => standardTimerToDeath;



    public abstract void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats);
    public abstract void CalculateDamage();


    public virtual void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats, float timer)
    {
        transform.position = startingPosition.position;
        transform.rotation = startingPosition.rotation;
        TargetDamageable = _target;
        TimerToDeath = timer;
        Damage = towerStats.Damage;
        Speed = towerStats.ProjectileSpeed;
        gameObject.SetActive(true);
    }

    public virtual void Awake()
    {
        gameObject.SetActive(false);
        TargetDamageable = null;
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
    }

    // Update is called once per frame
    public virtual void Update()
    {

        if (TargetDamageable != null)
        {
            MoveProjectile();
            if (Vector3.Distance(transform.position, TargetDamageable.transform.position) < 1.5f)
            {
                CalculateDamage();
                AfterDamageBehavior();
            }
            TimerToDeath -= Time.deltaTime;

            if (TimerToDeath <= 0)
                TurnOffProjectile();
        }
        else
            TurnOffProjectile();


        //if (TargetDamageable != null)
        //{

        //    MoveDir = (TargetDamageable.transform.position - transform.position).normalized;

        //    transform.position += MoveDir * Speed * Time.deltaTime;

        //    if (Vector3.Distance(transform.position, TargetDamageable.transform.position) < 1.5f)
        //    {
        //        TargetDamageable.TakeDamage(Damage);
        //        gameObject.SetActive(false);
        //    }
        //    TimerToDeath -= Time.deltaTime;

        //    if (TimerToDeath <= 0)            
        //        gameObject.SetActive(false);
            

        //}
        //else        
        //    gameObject.SetActive(false);
    }

    public virtual void MoveProjectile()
    {
        MoveDir = (TargetDamageable.transform.position - transform.position).normalized;
        transform.position += MoveDir * Speed * Time.deltaTime;
    }

    public virtual void TurnOffProjectile()
    {
        gameObject.SetActive(false);
    }

    public virtual void AfterDamageBehavior()
    {
        TurnOffProjectile();
    }


}
