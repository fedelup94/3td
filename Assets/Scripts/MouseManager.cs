﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class MouseManager : Singleton<MouseManager>
{

    private EventSystem eventSystem;

    public EventSystem EventSystem { get => eventSystem; set => eventSystem = value; }

    public override void Awake()
    {
        base.Awake();
        EventSystem = GetComponent<EventSystem>();

    }

    public bool IsMouseOverUI()
    {
        return EventSystem.IsPointerOverGameObject();
    }


}
