﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Air", menuName = "CustomSO/Enemy/Modifier/AirModifier")]
public class AirModifier : EnemyModifier<Enemy>
{
    public override void AfterDamageTakenResolveModifier(Enemy target)
    {
    }

    public override void BeforeDamageTakenResolveModifier(Enemy target)
    {
    }

    public override void OnDeathResolveModifier(Enemy target)
    {
    }

    public override void OnStartResolveModifier(Enemy target)
    {
        target.IsAirUnit = true;
    }

    public override void OnUpdateResolveModifier(Enemy target)
    {

    }
}
