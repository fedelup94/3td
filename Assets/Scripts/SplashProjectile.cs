﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashProjectile : Projectile
{

    private float splash;
    private float splashRange;

    public float Splash { get => splash; set => splash = value; }
    public float SplashRange { get => splashRange; set => splashRange = value; }

    public override void Awake()
    {
        base.Awake();
    }

    
    public override void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats)
    {
        ShootProjectile(startingPosition, _target, towerStats, StandardTimerToDeath);
    }

    public override void ShootProjectile(Transform startingPosition, Enemy _target, DamageTowerType towerStats, float timer)
    {
        transform.position = startingPosition.position;
        transform.rotation = startingPosition.rotation;
        TargetDamageable = _target;
        TimerToDeath = timer;
        Damage = towerStats.Damage;
        Speed = towerStats.ProjectileSpeed;
        Splash = towerStats.Splash;
        SplashRange = towerStats.SplashRange;
        gameObject.SetActive(true);
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void CalculateDamage()
    {
        TargetDamageable.TakeDamage(Damage);

        foreach (var splashEnemy in GetAllSplashTarget())
            splashEnemy.TakeDamage(Splash);

    }


    //TODO FIX THIS METHOD
    // Non mi piace mi sembra molto pesante --> DA MIGLIORARE
    private List<Enemy> GetAllSplashTarget()
    {
        List<Enemy> targetList = new List<Enemy>();

        var colliders = Physics.OverlapSphere(TargetDamageable.transform.position, SplashRange, LayerMask.GetMask("Enemy"));
        foreach (var collider in colliders)        
            targetList.Add(collider.GetComponent<Enemy>());

        if (targetList.Contains(TargetDamageable))
            targetList.Remove(TargetDamageable);


        return targetList;
    }
}
