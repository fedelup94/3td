﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : SingletonPersistent<GameAssets>
{

    #region TowerRelatedPrefabs
    [Header("Tower Prefabs")]
    [SerializeField]
    private GameObject arrowTower;

    [SerializeField]
    private GameObject cannonTower;

    [SerializeField]
    private GameObject missileTower;

    [SerializeField]
    private GameObject timeshiftTower;

    [SerializeField]
    private GameObject teslaTower;

    [SerializeField]
    private GameObject laserTower;

    [SerializeField]
    private GameObject sniperTower;

    [Header("Tower Projectiles")]
    [SerializeField]
    private GameObject projectilePrefab;

    public GameObject ProjectilePrefab { get => projectilePrefab; set => projectilePrefab = value; }
    public GameObject ArrowTower { get => arrowTower; set => arrowTower = value; }
    public GameObject CannonTower { get => cannonTower; set => cannonTower = value; }
    public GameObject MissileTower { get => missileTower; set => missileTower = value; }
    public GameObject TimeshiftTower { get => timeshiftTower; set => timeshiftTower = value; }
    public GameObject TeslaTower { get => teslaTower; set => teslaTower = value; }
    public GameObject LaserTower { get => laserTower; set => laserTower = value; }
    public GameObject SniperTower { get => sniperTower; set => sniperTower = value; }
    #endregion


    public override void Awake()
    {
        base.Awake();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }


    public GameObject GetTowerFromAssets(TOWER_TYPE type, Transform newParent)
    {
        var tower = Instantiate(Utils.GetTowerPrefabFromType(type), newParent.position, new Quaternion(), newParent);

        switch (type)
        {
            case TOWER_TYPE.ARROW:
                tower.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y + 5, tower.transform.position.z);
                break;
            case TOWER_TYPE.CANNON:
                tower.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y + 1, tower.transform.position.z);
                break;
            case TOWER_TYPE.MISSILE:
                break;
            case TOWER_TYPE.TIME_SHIFT:
                break;
            case TOWER_TYPE.TESLA:
                break;
            case TOWER_TYPE.LASER:
                break;
            case TOWER_TYPE.SNIPER:
                tower.transform.position = new Vector3(tower.transform.position.x, tower.transform.position.y, tower.transform.position.z);
                break;
            default:
                break;
        }


        return tower;
    }
}
