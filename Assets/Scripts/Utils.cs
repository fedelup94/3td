﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{

    public static GameObject GetTowerPrefabFromType(TOWER_TYPE type)
    {
        switch (type)
        {
            case TOWER_TYPE.ARROW:
                return GameAssets.Instance.ArrowTower;
            case TOWER_TYPE.CANNON:
                return GameAssets.Instance.CannonTower;
            case TOWER_TYPE.MISSILE:
                return GameAssets.Instance.MissileTower;
            case TOWER_TYPE.TIME_SHIFT:
                return GameAssets.Instance.TimeshiftTower;
            case TOWER_TYPE.TESLA:
                return GameAssets.Instance.TeslaTower;
            case TOWER_TYPE.LASER:
                return GameAssets.Instance.LaserTower;
            case TOWER_TYPE.SNIPER:
                return GameAssets.Instance.SniperTower;
            default:
                return null;                
        }
    }


    public static TextMesh CreateWorldText(string text, Transform parent = null, Vector3 localPosition = default(Vector3), int fontSize = 40, Color? color = null, TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Left, int sortingOrder = 5000)
    {
        color = (color == null) ? Color.white : color;
        return CreateWorldText(parent, text, localPosition, fontSize, (Color)color, textAnchor, textAlignment, sortingOrder);
    }

    public static TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, TextAnchor textAnchor, TextAlignment textAlignment, int sortingOrder)
    {
        GameObject gameObject = new GameObject("World Text", typeof(TextMesh));
        Transform transform = gameObject.transform;
        transform.SetParent(parent, false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();

        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;

        // Rotate on X
        var angle = transform.rotation.eulerAngles;
        angle.x = 90;

        transform.rotation = Quaternion.Euler(angle);
        return textMesh;
    }


}
