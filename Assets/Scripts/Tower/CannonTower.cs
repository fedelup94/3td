﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonTower : DamageTower
{

    public override void Awake()
    {
        base.Awake();
        Priority = TargetPriority.Strongest;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    public override void Start()
    {
        base.Start();

    }

    public override void Update()
    {
        base.Update();
        CleanTarget();
        if (Target.Count > 0)
        {
            Attack();
        }
        if (Target.Count < NumberOfTarget)
            FindTarget();
    }

    public override Projectile GetPooledProjectile()
    {
        return ObjectPooler.Instance.GetPooledSplashProjectile();
    }
}
