﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TOWER_TYPE { ARROW, CANNON, MISSILE, TIME_SHIFT, TESLA, LASER, SNIPER };


[CreateAssetMenu(fileName = "New Damage Tower", menuName = "CustomSO/DamageTowerType")]
public class DamageTowerType : ScriptableObject
{

    [Header("Stats")]
    [SerializeField]
    private TOWER_TYPE type;

    [SerializeField]
    private float damage;

    [SerializeField]
    private float attacksPerSecond;

    [SerializeField]
    private float splash;

    [SerializeField]
    private float splashRange;

    [SerializeField]
    private float range;

    [SerializeField]
    private uint numberOfTargets;

    [SerializeField]
    private float projectileSpeed;

    [SerializeField]
    private bool canTargetAir;

    [Header("Cost Related")]
    [SerializeField]
    private GameObject upgrade;

    [SerializeField]
    private uint cost;




    public float Damage { get => damage; private set => damage = value; }
    public float Splash { get => splash; private set => splash = value; }
    public float Range { get => range; private set => range = value; }
    public GameObject Upgrade { get => upgrade; private set => upgrade = value; }
    public bool CanTargetAir { get => canTargetAir; private set => canTargetAir = value; }
    public uint NumberOfTargets { get => numberOfTargets; private set => numberOfTargets = value; }
    public float AttacksPerSecond { get => attacksPerSecond; private set => attacksPerSecond = value; }
    public float ProjectileSpeed { get => projectileSpeed; private set => projectileSpeed = value; }
    public float SplashRange { get => splashRange; set => splashRange = value; }
    public TOWER_TYPE Type { get => type; set => type = value; }
    public uint Cost { get => cost; set => cost = value; }
}
